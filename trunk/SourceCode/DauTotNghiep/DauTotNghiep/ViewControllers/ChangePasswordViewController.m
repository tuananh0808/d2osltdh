//
//  ChangePasswordViewController.m
//  DauTotNghiep
//
//  Created by 507-0 on 3/10/17.
//  Copyright © 2017 D002. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "ASTextField.h"
#import "UIButton+Glossy.h"

@interface ChangePasswordViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *changePasswordTableView;
@property (strong, nonatomic) IBOutlet UIView *changePasswordMain;
//cells
@property (strong, nonatomic) IBOutlet UITableViewCell *titlteCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *oldPasswordCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *nwPasswordCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *confirmPasswordCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *doneChangePassCell;
@property (strong, nonatomic) IBOutlet UITableViewCell *cancelChangePassCell;
// fields
@property (weak, nonatomic) IBOutlet UITextField *oldPassField;
@property (weak, nonatomic) IBOutlet UITextField *nwPassField;
@property (weak, nonatomic) IBOutlet UITextField *confirmNwPassField;
//buttons
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (weak, nonatomic) IBOutlet UILabel *titleChangePassLabel;
@property (strong, nonatomic) NSMutableArray *cellArr;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.cellArr = [NSMutableArray arrayWithObjects:_titlteCell,_oldPasswordCell,_nwPasswordCell,_confirmPasswordCell,_doneChangePassCell,_cancelChangePassCell, nil];
    
    //setup text field with respective icons
    [_oldPassField setupTextFieldWithType:ASTextFieldTypeRound withIconName:nil];
    [_confirmNwPassField setupTextFieldWithType:ASTextFieldTypeRound withIconName:nil];
    [_nwPassField setupTextFieldWithType:ASTextFieldTypeRound withIconName:nil];
    [self setTextFieldAndLabel];
}

- (void) setButton {
    //set login button
    self.doneButton.backgroundColor = [UIColor colorWithRed:190.0/255.0 green:140.0/255.0 blue:110.0/255.0 alpha:1.0f];
    [self.doneButton makeGlossy];
    //set cancel login button
    self.cancelButton.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:173.0/255.0 blue:173.0/255.0 alpha:1.0f];
    
    [self.cancelButton makeGlossy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)donePressed:(id)sender {
}
- (IBAction)cancelPressed:(id)sender {
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //return cell's height for particular row
    return ((UITableViewCell*)[self.cellArr objectAtIndex:indexPath.row]).frame.size.height;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return number of cells for the table
    return self.cellArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    //return cell for particular row
    cell = [self.cellArr objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    //set clear color to cell.
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (void)setTextFieldAndLabel{
    // set oldPassField
    self.oldPassField.backgroundColor = [UIColor colorWithRed:102.0/255.0 green:87.0/255.0 blue:83.0/255.0 alpha:1.0f];
    self.oldPassField.layer.cornerRadius = 15;
    self.oldPassField.layer.borderWidth = 1.0f;
    //set nwPassField
    self.nwPassField.backgroundColor= [UIColor colorWithRed:102.0/255.0 green:87.0/255.0 blue:83.0/255.0 alpha:1.0f];
    self.nwPassField.layer.cornerRadius = 15;
    self.nwPassField.layer.borderWidth = 1.0f;
    //set titleChangePassLabel
    self.titleChangePassLabel.textColor = [UIColor whiteColor];
    self.titleChangePassLabel.backgroundColor = [UIColor colorWithRed:147.0/255.0 green:107.0/255.0 blue:92.0/255.0 alpha:1.0f];
    self.titleChangePassLabel.layer.borderWidth = 1.0f;
    self.titleChangePassLabel.clipsToBounds = YES;
    //set confirmNwPassField
    self.confirmNwPassField.layer.cornerRadius = 15;
    self.confirmNwPassField.layer.borderWidth = 1.0f;
    self.confirmNwPassField.backgroundColor= [UIColor colorWithRed:102.0/255.0 green:87.0/255.0 blue:83.0/255.0 alpha:1.0f];
}
- (void) setBackground{
    // set backgroundAlert
    self.changePasswordTableView.layer.borderWidth = 1.0f;
    self.changePasswordTableView.layer.cornerRadius = 15.0f;
    self.changePasswordTableView.clipsToBounds = YES;
    
}
@end
