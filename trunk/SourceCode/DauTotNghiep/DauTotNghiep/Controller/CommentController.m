//
//  CommentController.m
//  DauTotNghiep
//
//  Created by test on 3/10/17.
//  Copyright © 2017 D002. All rights reserved.
//

#import "CommentController.h"
#import "InformationComment.h"
#import "APIController.h"
#import "Def.h"
#import "MulticastDelegate.h"

@implementation CommentController
+(id) getInstance{
    static CommentController *getInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getInstance = [[self alloc] initConnection];
    });
    return getInstance;
}
- (id) initConnection
{
    if(self = [super init])
    {
        multicastDelegate = [[MulticastDelegate alloc] init];
        self.connection = [APIController shareInstance];
    }
    
    return self;
}
- (void)addDelegate:(id)delegate
{
    [multicastDelegate addDelegate:delegate];
}

-(NSURLSessionDataTask *) getCommentWithMa_de:(NSString *)maDe {
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: maDe, ma_de, nil];
    NSURLSessionDataTask *requestTask = [[APIController shareInstance] requestAPI:GET_COMMENT
                                                                           method: POST_METHOD
                                                                       parameters: dict
                                                                          success: ^(NSURLSessionDataTask *task, id responseObject)
                                         {
                                            if(self.delegate && [self.delegate respondsToSelector:@selector(didRequestComment:taskRequest:error:)])
                                            {
                                                [self.delegate didRequestComment:responseObject taskRequest:task error:nil];
                                            }
                                                
                                             }
                                         
                                                                          failure:^(NSURLSessionDataTask *task, NSError *error)
                                         {
                                             if(self.delegate && [self.delegate respondsToSelector:@selector(didRequestComment:taskRequest:error:)])
                                             {
                                                 [self.delegate didRequestComment:nil taskRequest:task error:error];
                                             }
                                         }];
    return requestTask;
}
@end
