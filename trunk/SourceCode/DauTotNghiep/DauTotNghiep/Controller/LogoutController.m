//
//  LogoutController.m
//  DauTotNghiep
//
//  Created by Nguyễn Hưng on 3/10/17.
//  Copyright © 2017 D002. All rights reserved.
//

#import "LogoutController.h"
#import "MulticastDelegate.h"
#import "Def.h"

@implementation LogoutController
+(id) getInstance{
    static LogoutController *getInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        getInstance = [[self alloc] initConnection];
    });
    return getInstance;
}
- (id) initConnection
{
    if(self = [super init])
    {
        multicastDelegate = [[MulticastDelegate alloc] init];
        self.connection = [APIController shareInstance];
    }
    
    return self;
}
- (void)addDelegate:(id)delegate
{
    [multicastDelegate addDelegate:delegate];
}
-(NSURLSessionDataTask *) postLogout: (User *) user;
{
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: user.email, kEmail, nil];
    NSURLSessionDataTask *requestTask = [[APIController shareInstance] requestAPI:APIPostLogout
                                                                           method: POST_METHOD
                                                                       parameters: dict
                                                                          success: ^(NSURLSessionDataTask *task, id responseObject)
                                         {
                                             int resultCode = [[responseObject objectForKey:kResultCodeKeyName]intValue];
                                             if (resultCode == kRequestOK && multicastDelegate && [multicastDelegate respondsToSelector:@selector(didRequestLogout:taskRequest:error:errorObject:)]) {
                                                 User *user = [[User alloc] init];
                                                 [multicastDelegate didRequestLogout:user taskRequest:task error:nil errorObject:nil];
                                                 
                                             }
                                             else if (multicastDelegate && [multicastDelegate respondsToSelector:@selector(didRequestLogout:taskRequest:error:errorObject:)])
                                             {
                                                 [multicastDelegate didRequestLogout:nil taskRequest:task error:nil errorObject:responseObject];
                                             }
                                         }
                                                                          failure:^(NSURLSessionDataTask *task, NSError *error)
                                         {
                                             if (multicastDelegate && [multicastDelegate respondsToSelector:@selector(didRequestLogout:taskRequest:error:errorObject:)])
                                             {
                                                 [multicastDelegate didRequestLogout:nil taskRequest:task error:error errorObject:nil];
                                             }
                                         }];
    return requestTask;
}
@end
