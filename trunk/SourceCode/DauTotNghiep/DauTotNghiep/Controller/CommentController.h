//
//  CommentController.h
//  DauTotNghiep
//
//  Created by test on 3/10/17.
//  Copyright © 2017 D002. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIController.h"
#import "InformationComment.h"
@protocol CommentControllerDelegate<NSObject>

-(void) didRequestComment: (NSDictionary *)infor taskRequest: (NSURLSessionDataTask *)taskRequest error: (NSError *)error;
@end
@interface CommentController : NSObject{
    id multicastDelegate;
}


-(NSURLSessionDataTask *) getCommentWithMa_de:(NSString *)maDe ;
@property (nonatomic) APIController *connection;
- (void)addDelegate:(id)delegate;
+(id) getInstance;
@property (nonatomic, weak) id<CommentControllerDelegate> delegate;
@end
