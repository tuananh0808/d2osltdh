//
//  LogoutController.h
//  DauTotNghiep
//
//  Created by Nguyễn Hưng on 3/10/17.
//  Copyright © 2017 D002. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIController.h"
#import "User.h"

@protocol LogoutControllerDelegate <NSObject>
@optional
-(void) didRequestLogout: (User *) user taskRequest: (NSURLSessionDataTask *)taskRequest error: (NSError *)error errorObject: (id)errorObject;
@end
@interface LogoutController : NSObject
{
    id multicastDelegate;
}
-(NSURLSessionDataTask *) postLogout: (User *) user;
@property (nonatomic) APIController *connection;
- (void)addDelegate:(id)delegate;
+(id) getInstance;
@end
