//
//  InformationComment.m
//  DauTotNghiep
//
//  Created by test on 3/1/17.
//  Copyright © 2017 D002. All rights reserved.
//

#import "InformationComment.h"

@implementation InformationComment
-(id)initWithComment :(NSString *)comment email:(NSString *)email{
    if(self = [super init]){
        self.emails = email;
        self.textComment = comment;
    }
    return self;
}
@end
